package matching;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;

class LongestCommonSubstring {

	private String s1, s2;
	private int positionInS1, positionInS2, length;

	

	public LongestCommonSubstring(String s1, String s2) {
		this.setS1(s1);
		this.setS2(s2);
		this.solveDynamically(this.getS1(), this.getS2());
		this.trim();
	}

	private void solveDynamically(String S1, String S2) {
		int p1 = 0;
		int maxLength = 0;
		
		int p2 = 0;
		
		for (int i = 0; i < S1.length(); i++) {
			for (int j = 0; j < S2.length(); j++) {
				int x = 0;
				while (S1.charAt(i + x) == S2.charAt(j + x)) {
					x++;
					if (((i + x) >= S1.length()) || ((j + x) >= S2.length()))
						break;
				}
				if (x > maxLength) {
					maxLength = x;
					p1 = i;
					p2 = j;
				}
			}
		}
		this.setPositionInFirstString(p1);
		this.setPositionInSecondString(p2);
		this.setLength(maxLength);

	}
	
	private void trim(){
		while(this.getSolution().contains(" ") && !this.getSolution().startsWith(" ") && this.getPositionInFirstString() != 0){
			this.setPositionInFirstString(this.getPositionInFirstString() + 1);
			this.setPositionInSecondString(this.getPositionInSecondString() + 1);
			this.setLength(this.getLength() - 1);
		}
		while(this.getSolution().startsWith(" ")){
			this.setPositionInFirstString(this.getPositionInFirstString() + 1);
			this.setPositionInSecondString(this.getPositionInSecondString() + 1);
			this.setLength(this.getLength() - 1);
		}
		while(this.getSolution().contains(" ") &&  !this.getSolution().endsWith(" ")){
			this.setLength(this.getLength() - 1);
		}
		while(this.getSolution().endsWith(" ")){
			this.setLength(this.getLength() - 1);
		}
	}

	public int getPositionInFirstString() {
		return this.positionInS1;
	}
	
	public void setPositionInFirstString(int p){
		this.positionInS1 = p;
	}
	
	public int getPositionInSecondString() {
		return positionInS2;
	}

	private void setPositionInSecondString(int p) {
		this.positionInS2 = p;
	}

	public int getLength() {
		return this.length;
	}

	private String getS1() {
		return s1;
	}

	private void setS1(String s1) {
		this.s1 = s1;
	}

	private String getS2() {
		return s2;
	}

	private void setS2(String s2) {
		this.s2 = s2;
	}

	public String getSolution() {
		return this.getS1().substring(this.getPositionInFirstString(),
				this.getPositionInFirstString() + this.getLength());
	}



	private void setLength(int length) {
		this.length = length;
	}
	
	@Override
	public String toString(){
		return this.getSolution();
	}

}
