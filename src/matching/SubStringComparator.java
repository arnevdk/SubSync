package matching;

import java.util.ArrayList;
import java.util.List;

import components.LinearScale;
import components.TimedString;

class SubStringComparator extends TimedStringComparator {

	private static final int NUM_SAMPLES = 5;
	
	private List<String> substrList;

	private SubStringComparator(TimedString ts1, TimedString ts2) {
		super(ts1, ts1);
		this.substrList = new ArrayList<String>(NUM_SAMPLES);
	}

	protected LinearScale compare() {
		this.addPoints();
		return null;
	}

	private void addPoints() {
		TimedString s1 = this.getTs1().copy();
		TimedString s2 = this.getTs2().copy();

		for (int samp = 0; samp < NUM_SAMPLES; samp++) {
			LongestCommonSubstring lcs = new LongestCommonSubstring(s1.getImplodedString(), s2.getImplodedString());
			for (int i = 0; i < lcs.getLength(); i++) {
				long correctTime = this.getTs1().getPositionMap().get(lcs.getPositionInFirstString() + i);
				long actualTime = this.getTs2().getPositionMap().get(lcs.getPositionInSecondString() + i);
				this.getPointMap().put(correctTime, actualTime);
			}
			for (int i = 0; i < lcs.getLength(); i++) {
				s1.remove(lcs.getPositionInFirstString() + i);
				s2.remove(lcs.getPositionInSecondString() + i);
			}
			substrList.add(lcs.getSolution());
		}

	}

}
