package matching;

import java.util.HashMap;
import java.util.Map;

import components.LinearScale;
import components.TimedString;

abstract class TimedStringComparator {
	private TimedString ts1, ts2;
	private Map<Long, Long> pointMap;
	private LinearScale linearScale;
	
	public TimedStringComparator(TimedString ts1, TimedString ts2){
		this.setTs1(ts1);
		this.setTs1(ts2);
		this.setPointMap(new HashMap<Long, Long>());
		this.setLinearScale(this.compare());
	}
	
	protected abstract LinearScale compare();
	
	protected TimedString getTs1() {
		return ts1;
	}

	private void setTs1(TimedString ts1) {
		this.ts1 = ts1;
	}

	protected TimedString getTs2() {
		return ts2;
	}

	private void setTs2(TimedString ts2) {
		this.ts2 = ts2;
	}

	protected Map<Long, Long> getPointMap() {
		return pointMap;
	}

	protected void setPointMap(Map<Long, Long> pointMap) {
		this.pointMap = pointMap;
	}

	public LinearScale getLinearScale() {
		return linearScale;
	}

	protected void setLinearScale(LinearScale linearScale) {
		this.linearScale = linearScale;
	}


}
